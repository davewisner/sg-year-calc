﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PeopleListManager : MonoBehaviour {

    public GameObject personListItemPrefab;
    public Transform contentPanel;
    public EditPerson editPerson;

    List<PersonData> people = new List<PersonData>();
    List<GameObject> personListItems = new List<GameObject>();

    public PersonListItem selectedItem;
    
    // Use this for initialization
	void Start () {
        LoadDefaultPeople();

        ClearAndRefreshList();
	}

    public List<PersonData> GetPeople() { return people; }

    public void ClearAndRefreshList()
    {
        for (int i = 0; i < personListItems.Count; i++)
        {
            Destroy(personListItems[i]);
        }

        personListItems.Clear();

        for (int i = 0; i < people.Count; i++)
        {
            PersonData person = people[i];
            AddPersonToList(person, i);
        }
    }

    private void AddPersonToList(PersonData person, int index)
    {
        GameObject newPersonListItem = (GameObject)GameObject.Instantiate(personListItemPrefab);
        newPersonListItem.transform.SetParent(contentPanel, false);

        PersonListItem personListItem = newPersonListItem.GetComponent<PersonListItem>();
        personListItem.Populate(person, this, index);

        personListItems.Add(newPersonListItem);
    }

    public void SelectItem(PersonListItem newSelectedItem)
    {
        for (int i = 0; i < personListItems.Count; i++)
        {
            GameObject item = personListItems[i];
            if (item == newSelectedItem.gameObject)
            {
                selectedItem = newSelectedItem;
            }
            else
            {
                item.GetComponent<PersonListItem>().Deselect();
            }
        }

        PersonData person = people[selectedItem.listIndex];
        editPerson.Show(person);
    }

    public void DeselectItem()
    {
        if (selectedItem != null)
        {
            selectedItem.Deselect();
            selectedItem = null;
        }
    }

    public void SaveItem(PersonData person)
    {
        int index;
        if (selectedItem != null)
        {
            index = selectedItem.listIndex;
            people[index].birthDate = person.birthDate;
            people[index].deathDate = person.deathDate;
            selectedItem.Populate(person, this, index);
        }
        else
        {
            people.Add(person);
            AddPersonToList(person, people.Count - 1);
        }
    }

    public void DeleteSelectedItem()
    {
        if (selectedItem != null)
        {
            int index = selectedItem.listIndex;
            personListItems.Remove(selectedItem.gameObject);
            Destroy(selectedItem.gameObject);
            selectedItem = null;
            people.RemoveAt(index);
            
            for (int i = 0; i < personListItems.Count; i++)
            {
                PersonListItem personListItem = personListItems[i].GetComponent<PersonListItem>();
                if (personListItem.listIndex > index)
                    personListItem.listIndex--;
            }

            editPerson.Hide();
        }
    }

    public void HideEditPanel()
    {
        editPerson.Hide();
    }


    #region Default Data

    private void LoadDefaultPeople()
    {
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1922"), deathDate = DateTime.Parse("1/1/1961") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1930"), deathDate = DateTime.Parse("1/1/1957") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1908"), deathDate = DateTime.Parse("1/1/1934") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1999"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1915"), deathDate = DateTime.Parse("1/1/1996") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1924"), deathDate = DateTime.Parse("1/1/1970") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1901"), deathDate = DateTime.Parse("1/1/1946") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1945"), deathDate = DateTime.Parse("1/1/1952") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1998"), deathDate = DateTime.Parse("1/1/1999") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1993"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1962"), deathDate = DateTime.Parse("1/1/1972") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1903"), deathDate = DateTime.Parse("1/1/1967") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1985"), deathDate = DateTime.Parse("1/1/1997") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1988"), deathDate = DateTime.Parse("1/1/1989") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1973"), deathDate = DateTime.Parse("1/1/1977") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1995"), deathDate = DateTime.Parse("1/1/1996") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1940"), deathDate = DateTime.Parse("1/1/1946") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1900"), deathDate = DateTime.Parse("1/1/1967") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1923"), deathDate = DateTime.Parse("1/1/1976") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1945"), deathDate = DateTime.Parse("1/1/1975") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1907"), deathDate = DateTime.Parse("1/1/1993") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1906"), deathDate = DateTime.Parse("1/1/1999") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1917"), deathDate = DateTime.Parse("1/1/1931") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1915"), deathDate = DateTime.Parse("1/1/1956") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1960"), deathDate = DateTime.Parse("1/1/1975") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1941"), deathDate = DateTime.Parse("1/1/1944") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1915"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1993"), deathDate = DateTime.Parse("1/1/1996") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1951"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1903"), deathDate = DateTime.Parse("1/1/1954") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/2000"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1995"), deathDate = DateTime.Parse("1/1/1998") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1958"), deathDate = DateTime.Parse("1/1/1986") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1948"), deathDate = DateTime.Parse("1/1/1966") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1968"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1922"), deathDate = DateTime.Parse("1/1/1932") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1999"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1942"), deathDate = DateTime.Parse("1/1/1998") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1933"), deathDate = DateTime.Parse("1/1/1942") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1977"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1935"), deathDate = DateTime.Parse("1/1/1995") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/2000"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1988"), deathDate = DateTime.Parse("1/1/1998") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1934"), deathDate = DateTime.Parse("1/1/1988") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1965"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1961"), deathDate = DateTime.Parse("1/1/1995") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1941"), deathDate = DateTime.Parse("1/1/1992") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1971"), deathDate = DateTime.Parse("1/1/1975") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1961"), deathDate = DateTime.Parse("1/1/1981") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1989"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1951"), deathDate = DateTime.Parse("1/1/1999") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1942"), deathDate = DateTime.Parse("1/1/1988") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1997"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1946"), deathDate = DateTime.Parse("1/1/1968") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1925"), deathDate = DateTime.Parse("1/1/1987") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1907"), deathDate = DateTime.Parse("1/1/1914") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1935"), deathDate = DateTime.Parse("1/1/1940") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1914"), deathDate = DateTime.Parse("1/1/1999") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1959"), deathDate = DateTime.Parse("1/1/1961") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1975"), deathDate = DateTime.Parse("1/1/1979") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1947"), deathDate = DateTime.Parse("1/1/1956") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1917"), deathDate = DateTime.Parse("1/1/1991") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1901"), deathDate = DateTime.Parse("1/1/1946") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1966"), deathDate = DateTime.Parse("1/1/1967") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1945"), deathDate = DateTime.Parse("1/1/1984") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1930"), deathDate = DateTime.Parse("1/1/1983") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1905"), deathDate = DateTime.Parse("1/1/1981") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1935"), deathDate = DateTime.Parse("1/1/1985") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1906"), deathDate = DateTime.Parse("1/1/1945") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1935"), deathDate = DateTime.Parse("1/1/1979") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1939"), deathDate = DateTime.Parse("1/1/1952") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1997"), deathDate = DateTime.Parse("1/1/1998") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1980"), deathDate = DateTime.Parse("1/1/1985") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1933"), deathDate = DateTime.Parse("1/1/1990") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1939"), deathDate = DateTime.Parse("1/1/1975") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1930"), deathDate = DateTime.Parse("1/1/1945") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1948"), deathDate = DateTime.Parse("1/1/1995") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1962"), deathDate = DateTime.Parse("1/1/1990") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1953"), deathDate = DateTime.Parse("1/1/1967") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1985"), deathDate = DateTime.Parse("1/1/1985") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1986"), deathDate = DateTime.Parse("1/1/1995") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1967"), deathDate = DateTime.Parse("1/1/1975") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1963"), deathDate = DateTime.Parse("1/1/1993") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1923"), deathDate = DateTime.Parse("1/1/1941") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1996"), deathDate = DateTime.Parse("1/1/1997") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1904"), deathDate = DateTime.Parse("1/1/1971") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1962"), deathDate = DateTime.Parse("1/1/1998") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1933"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1961"), deathDate = DateTime.Parse("1/1/1984") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1915"), deathDate = DateTime.Parse("1/1/1988") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1939"), deathDate = DateTime.Parse("1/1/1944") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1933"), deathDate = DateTime.Parse("1/1/1961") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1952"), deathDate = DateTime.Parse("1/1/1995") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1916"), deathDate = DateTime.Parse("1/1/1984") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1905"), deathDate = DateTime.Parse("1/1/1963") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1948"), deathDate = DateTime.Parse("1/1/1974") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1978"), deathDate = DateTime.Parse("1/1/1998") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1972"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1909"), deathDate = DateTime.Parse("1/1/1965") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1916"), deathDate = DateTime.Parse("1/1/1941") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1960"), deathDate = DateTime.Parse("1/1/1980") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1967"), deathDate = DateTime.Parse("1/1/1990") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1917"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1974"), deathDate = DateTime.Parse("1/1/1975") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1921"), deathDate = DateTime.Parse("1/1/1952") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1987"), deathDate = DateTime.Parse("1/1/1990") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1905"), deathDate = DateTime.Parse("1/1/1979") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1998"), deathDate = DateTime.Parse("1/1/1999") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1910"), deathDate = DateTime.Parse("1/1/1923") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1943"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1987"), deathDate = DateTime.Parse("1/1/1993") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1928"), deathDate = DateTime.Parse("1/1/1933") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1977"), deathDate = DateTime.Parse("1/1/1989") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1926"), deathDate = DateTime.Parse("1/1/1957") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1933"), deathDate = DateTime.Parse("1/1/1956") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1929"), deathDate = DateTime.Parse("1/1/1932") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1985"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1960"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1930"), deathDate = DateTime.Parse("1/1/1986") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1930"), deathDate = DateTime.Parse("1/1/1941") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1940"), deathDate = DateTime.Parse("1/1/1980") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1913"), deathDate = DateTime.Parse("1/1/1918") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1909"), deathDate = DateTime.Parse("1/1/1947") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1966"), deathDate = DateTime.Parse("1/1/1978") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1932"), deathDate = DateTime.Parse("1/1/1936") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1902"), deathDate = DateTime.Parse("1/1/1956") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1907"), deathDate = DateTime.Parse("1/1/1915") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1996"), deathDate = DateTime.Parse("1/1/1998") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1994"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1970"), deathDate = DateTime.Parse("1/1/1971") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1986"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1904"), deathDate = DateTime.Parse("1/1/1973") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1963"), deathDate = DateTime.Parse("1/1/1991") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1906"), deathDate = DateTime.Parse("1/1/1984") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1968"), deathDate = DateTime.Parse("1/1/1977") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1919"), deathDate = DateTime.Parse("1/1/1925") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1980"), deathDate = DateTime.Parse("1/1/1994") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1917"), deathDate = DateTime.Parse("1/1/1967") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1919"), deathDate = DateTime.Parse("1/1/1937") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1918"), deathDate = DateTime.Parse("1/1/1931") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1927"), deathDate = DateTime.Parse("1/1/1938") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1960"), deathDate = DateTime.Parse("1/1/1980") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1947"), deathDate = DateTime.Parse("1/1/1951") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1981"), deathDate = DateTime.Parse("1/1/1999") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1997"), deathDate = DateTime.Parse("1/1/1998") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1990"), deathDate = DateTime.Parse("1/1/1999") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1917"), deathDate = DateTime.Parse("1/1/1953") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1978"), deathDate = DateTime.Parse("1/1/1985") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1951"), deathDate = DateTime.Parse("1/1/1960") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1992"), deathDate = DateTime.Parse("1/1/1993") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1900"), deathDate = DateTime.Parse("1/1/1980") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1954"), deathDate = DateTime.Parse("1/1/1991") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1988"), deathDate = DateTime.Parse("1/1/1991") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1968"), deathDate = DateTime.Parse("1/1/1978") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1990"), deathDate = DateTime.Parse("1/1/1997") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1988"), deathDate = DateTime.Parse("1/1/1992") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1931"), deathDate = DateTime.Parse("1/1/1950") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1981"), deathDate = DateTime.Parse("1/1/1997") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1982"), deathDate = DateTime.Parse("1/1/1992") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1959"), deathDate = DateTime.Parse("1/1/1972") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1912"), deathDate = DateTime.Parse("1/1/1955") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1979"), deathDate = DateTime.Parse("1/1/1990") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1961"), deathDate = DateTime.Parse("1/1/1962") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1946"), deathDate = DateTime.Parse("1/1/1966") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1990"), deathDate = DateTime.Parse("1/1/1999") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1961"), deathDate = DateTime.Parse("1/1/1978") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1908"), deathDate = DateTime.Parse("1/1/1972") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1942"), deathDate = DateTime.Parse("1/1/1976") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1900"), deathDate = DateTime.Parse("1/1/1959") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1954"), deathDate = DateTime.Parse("1/1/1963") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1934"), deathDate = DateTime.Parse("1/1/1948") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1963"), deathDate = DateTime.Parse("1/1/1975") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1975"), deathDate = DateTime.Parse("1/1/1980") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1970"), deathDate = DateTime.Parse("1/1/1973") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1951"), deathDate = DateTime.Parse("1/1/1952") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1990"), deathDate = DateTime.Parse("1/1/1990") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1905"), deathDate = DateTime.Parse("1/1/1929") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1947"), deathDate = DateTime.Parse("1/1/1995") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1993"), deathDate = DateTime.Parse("1/1/1995") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1908"), deathDate = DateTime.Parse("1/1/1974") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1989"), deathDate = DateTime.Parse("1/1/1990") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1913"), deathDate = DateTime.Parse("1/1/1937") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1926"), deathDate = DateTime.Parse("1/1/1937") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1906"), deathDate = DateTime.Parse("1/1/1991") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1910"), deathDate = DateTime.Parse("1/1/1956") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1963"), deathDate = DateTime.Parse("1/1/1998") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1944"), deathDate = DateTime.Parse("1/1/1952") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1922"), deathDate = DateTime.Parse("1/1/1922") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1911"), deathDate = DateTime.Parse("1/1/1937") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1961"), deathDate = DateTime.Parse("1/1/1976") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1989"), deathDate = DateTime.Parse("1/1/1997") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1914"), deathDate = DateTime.Parse("1/1/1987") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1941"), deathDate = DateTime.Parse("1/1/1997") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1936"), deathDate = DateTime.Parse("1/1/1951") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1988"), deathDate = DateTime.Parse("1/1/1999") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1995"), deathDate = DateTime.Parse("1/1/1995") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/2000"), deathDate = DateTime.Parse("1/1/2000") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1938"), deathDate = DateTime.Parse("1/1/1945") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/1924"), deathDate = DateTime.Parse("1/1/1972") });
        people.Add(new PersonData { birthDate = DateTime.Parse("1/1/2000"), deathDate = DateTime.Parse("1/1/2000") });
    }
    #endregion

}
