﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditPerson : MonoBehaviour {

    public InputField dobInput;
    public InputField dodInput;
    public Button saveButton;
    public Button deleteButton;

    public PeopleListManager peopleList;

    ColorBlock startColors;
    ColorBlock errorColors;
    Color errorColor = new Color(1, .8f, .8f);

    public void Start()
    {
        this.gameObject.SetActive(false);
        startColors = dobInput.colors;
        errorColors = startColors;
        errorColors.normalColor = errorColor;
    }

    public void Show(PersonData person)
    {
        dobInput.text = person.birthDate.ToShortDateString();
        dodInput.text = person.deathDate.ToShortDateString();

        deleteButton.interactable = true;

        this.gameObject.SetActive(true);
    }

    public void ShowNew()
    {
        peopleList.DeselectItem();

        Clear();
        deleteButton.interactable = false;

        this.gameObject.SetActive(true);
    }

    public void ValidateInput()
    {
        bool isValid = true;
        ClearValidationErrors();

        DateTime dob = new DateTime();
        if (dobInput.text == "" || !DateTime.TryParse(dobInput.text, out dob))
        {
            isValid = false;
            SetValidationError(dobInput);
        }

        DateTime dod = new DateTime();
        if (dodInput.text == "" || !DateTime.TryParse(dodInput.text, out dod))
        {
            isValid = false;
            SetValidationError(dodInput);
        }

        if (isValid)
        {
            PersonData person = new PersonData
            {
                birthDate = dob,
                deathDate = dod
            };
            peopleList.SaveItem(person);
            Close();
        }
        else
        {
            saveButton.interactable = false;
        }
    }

    void SetValidationError(InputField field)
    {
        field.colors = errorColors;
    }

    void ClearValidationErrors()
    {
        dobInput.colors = startColors;
        dodInput.colors = startColors;
    }

    public void SetSaveButtonState()
    {
        saveButton.interactable = (dobInput.text != "" && dodInput.text != "");
    }

    public void Close()
    {
        peopleList.DeselectItem();
        Hide();
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
        Clear();
    }

    private void Clear()
    {
        ClearValidationErrors();
        dobInput.text = "";
        dodInput.text = "";
    }
}
