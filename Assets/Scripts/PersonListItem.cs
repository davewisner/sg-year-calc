﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonListItem : MonoBehaviour {

    public Button button;
    public Text birthDateLabel;
    public Text deathDateLabel;
    public int listIndex;
    PersonData person;
    PeopleListManager peopleList;
    ColorBlock startColors;
    ColorBlock newColors;
    Color highlightColor;

	// Use this for initialization
	void Start () {
        startColors = button.colors;
        highlightColor = button.colors.highlightedColor;
        newColors = startColors;
        newColors.normalColor = highlightColor;
	}

    public void Populate(PersonData newPersonData, PeopleListManager newPeopleList, int newListIndex)
    {
        person = newPersonData;
        birthDateLabel.text = person.birthDate.Year.ToString();
        deathDateLabel.text = person.deathDate.Year.ToString();

        peopleList = newPeopleList;
        listIndex = newListIndex;
    }

    public void Select()
    {
        button.colors = newColors;
        peopleList.SelectItem(this);
    }

    public void Deselect()
    {
        button.colors = startColors;
    }
}
