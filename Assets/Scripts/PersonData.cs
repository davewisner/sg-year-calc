﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PersonData {
    public DateTime birthDate;
    public DateTime deathDate;
}
