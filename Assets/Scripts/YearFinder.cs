﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YearFinder : MonoBehaviour {

    public PeopleListManager peopleList;
    public Text resultText;
    public InputField endYearInput;
    int? endYear;

    ColorBlock startColors;
    ColorBlock errorColors;
    Color errorColor = new Color(1, .8f, .8f);

    struct AliveCountChange
    {
        public int year;
        public int change;
    }

    public void Start()
    {
        startColors = endYearInput.colors;
        errorColors = startColors;
        errorColors.normalColor = errorColor;
    }

    public void ValidateInput()
    {
        peopleList.DeselectItem();
        peopleList.HideEditPanel();

        bool isValid = true;

        int validEndYear;
        if (endYearInput.text == "")
        {
            endYear = null;
        }
        else
        {
            if (!int.TryParse(endYearInput.text, out validEndYear))
            {
                isValid = false;
                endYearInput.colors = errorColors;
            }
            else
            {
                endYear = validEndYear;
            }
        }

        if (peopleList.GetPeople().Count == 0)
        {
            isValid = false;
            resultText.text = "No list of people found.";
        }

        if (isValid)
            RunCalculation();
    }

    public void ClearValidationErrors()
    {
        endYearInput.colors = startColors;
    }

    public void RunCalculation()
    {
        List<int> years = GetYearsWithMostAlive();

        string message;

        if (years.Count == 0)
        {
            message = "Unable to find a match based on your end year.";
        }
        else if (years.Count == 1)
        {
            message = years[0].ToString();
        }
        else
        {
            years.Sort();
            message = "It's a tie between ";
            for (int i = 0; i < years.Count; i++)
            {
                message += years[i].ToString();
                if (i != years.Count - 1)
                    message += (i == years.Count - 2 ? " and " : ", ");
            }
        }

        resultText.text = message;
    }

    private List<int> GetYearsWithMostAlive()
    {
        List<PersonData> people = peopleList.GetPeople();

        List<AliveCountChange> changes = new List<AliveCountChange>();

        for (int i = 0; i < people.Count; i++)
        {
            PersonData person = people[i];
            if (!endYear.HasValue || person.birthDate.Year <= endYear)
            {
                changes.Add(new AliveCountChange
                {
                    year = person.birthDate.Year,
                    change = 1
                });
            }

            if (!endYear.HasValue || person.deathDate.Year <= endYear)
            {
                changes.Add(new AliveCountChange
                {
                    // year + 1 so person is counted as alive for the year they died.
                    year = person.deathDate.Year + 1,
                    change = -1
                });
            }
        }

        // sort by year, and then negative changes first as they are one year behind from the year + 1 above. Allows for inclusion for year of death.
        changes = changes.OrderBy(x => x.year).ThenBy(x => x.change).ToList();

        int currentHighcount = 0;
        int numberOfPeopleAlive = 0;
        int indexOfLastMax = 0;
        int? yearOfLastMax = null;
        List<int> years = new List<int>();

        for (int i = 0; i < changes.Count; i++)
        {
            AliveCountChange change = changes[i];
            numberOfPeopleAlive += change.change;

            // Last checked year was a max (add the range of years between last and this)
            if (indexOfLastMax == i - 1 && yearOfLastMax < change.year - 1)
            {
                years.AddRange(Enumerable.Range((int)yearOfLastMax + 1, change.year - (int)yearOfLastMax - 1));
            }

            if (numberOfPeopleAlive >= currentHighcount)
            {
                if (numberOfPeopleAlive > currentHighcount)
                {
                    years.Clear();
                }

                currentHighcount = numberOfPeopleAlive;
                years.Add(change.year);
                indexOfLastMax = i;
                yearOfLastMax = change.year;
            }

        }

        // If endYears is set, handle remaining years when needed
        int lastChangeIndex = changes.Count - 1;
        if (endYear.HasValue && indexOfLastMax == changes.Count - 1 && changes[lastChangeIndex].year < endYear)
            years.AddRange(Enumerable.Range((int)yearOfLastMax + 1, (int)endYear - (int)yearOfLastMax));

        return years;
    }
}
